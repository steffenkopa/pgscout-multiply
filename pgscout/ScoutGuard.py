import logging
import sys
import time
from threading import Lock

from pgscout.Scout import Scout
from pgscout.config import use_pgpool, cfg_get
from pgscout.stats import unregister_account, register_account
from pgscout.utils import load_pgpool_accounts, buffer_account

log = logging.getLogger(__name__)

pgpool_lock = Lock()

class ScoutGuard(object):

    def __init__(self, auth, username, password, job_queue, first_one):
        self.job_queue = job_queue
        self.active = False

        # Set up initial account
        initial_account = {
            'auth_service': auth,
            'username': username,
            'password': password
        }
        self.acc = self.init_scout(initial_account, first_one)
        self.active = True

    def init_scout(self, acc_data, first_one):
        return Scout(acc_data['auth_service'], acc_data['username'], acc_data['password'], self.job_queue, first_one)

    def run(self):
        while True:
            self.active = True
            self.acc.run()
            self.active = False

            # Scout disabled, probably (shadow)banned.
            if use_pgpool():
                self.swap_account()
            else:
                # We don't have a replacement account, so just wait a veeeery long time.
                time.sleep(60*60*24*1000)
                break

    def swap_account(self):
        if unregister_account(self.acc.username):
            self.acc.release(reason=self.acc.last_msg)
            self.acc.username = ''
        while True:
            pgpool_lock.acquire()
            try:
                new_acc = load_pgpool_accounts(1)
                if new_acc:
                    first_one = register_account(new_acc['username'])
                    if first_one:
                        buffer_account(new_acc, cfg_get('multiply') - 1)
                    log.info("Swapping bad account {} with new account {}".format(self.acc.username, new_acc['username']))
                    self.acc = self.init_scout(new_acc, first_one)
                    break
            finally:
                pgpool_lock.release()
            log.warning("Could not request new account from PGPool. Out of accounts? Retrying in 1 minute.")
            time.sleep(60)