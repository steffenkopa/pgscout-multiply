from threading import Lock

import logging

log = logging.getLogger(__name__)

statistics = {
    # Maps Pokemon ID to total number of scouts for this Pokemon type
    'pokemon': {},
    'accounts': {}
}
stats_lock = Lock()


def inc_for_pokemon(pokemon_id):
    stats_lock.acquire()
    num = statistics['pokemon'].get(pokemon_id, 0)
    num += 1
    statistics['pokemon'][pokemon_id] = num
    stats_lock.release()


def get_pokemon_stats():
    stats_lock.acquire()
    pstats = map(lambda (pid, count): {'pid': pid, 'count': count},
                 statistics['pokemon'].items())
    stats_lock.release()
    pstats.sort(key=lambda x: x['pid'])
    return pstats


def register_account(username):
    stats_lock.acquire()
    try:
        stats = statistics['accounts'].get(username)
        if stats:
            stats['in_use'] += 1
            return False
        else:
            statistics['accounts'][username] = {
                'in_use': 1
            }
            return True
    finally:
        stats_lock.release()


def unregister_account(username):
    stats_lock.acquire()
    try:
        stats = statistics['accounts'].get(username)
        if stats:
            stats['in_use'] -= 1
            unused = stats['in_use'] == 0
            if unused:
                del statistics['accounts'][username]
            return unused
        else:
            log.error("No stats for {} on unregister".format(username))
    finally:
        stats_lock.release()
    return False